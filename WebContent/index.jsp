<html>
<head>
<!-- Include head links -->
<%@ include file="WEB-INF/jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
</head>
<body>
	<!-- Include header -->
	<%@ include file="/WEB-INF/jspf/header.content.jspf"%>
	<!-- Jumbotron landing image -->
	<div class="jumbotron landing-image">
		<div class="container landing-container">
			<h1 class="landing-text">Software Essentials BLOG</h1>
		</div>
	</div>

	<div class="container">
		<div class="row">
		<!-- Not logged in users -->
		<% if( userBean.getUserName() == null) {  %>
			<div class="col-md-6">
				<h2>Create an account</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-orange" href="./register" role="button">Register
						&raquo;</a>
				</p>
			</div>
			<div class="col-md-6">
				<h2>Login</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-orange" href="./login" role="button">Login
						&raquo;</a>
				</p>
			</div>
		<% } %>	
		<!-- Logged in users -->
		<% if( userBean.getUserName() != null) {  %>
			<div class="col-md-6">
				<h2>Create a post</h2>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus. Etiam porta sem malesuada
					magna mollis euismod. Donec sed odio dui.</p>
				<p>
					<a class="btn btn-orange" href="./create" role="button">Create
						&raquo;</a>
				</p>
			</div>
			<div class="col-md-6">
				<h2>Watch posts</h2>
				<p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in,
					quam. Vestibulum id ligula porta felis euismod semper. Fusce
					dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh,
					ut fermentum massa justo sit amet risus.</p>
				<p>
					<a class="btn btn-orange" href="./blog" role="button">Blog
						&raquo;</a>
				</p>
			</div>
		<% } %>
		</div>
		<!-- Footer -->
		<%@ include file="/WEB-INF/jspf/footer.content.jspf"%>
	</div>
</body>
</html>