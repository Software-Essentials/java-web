<html>
<head>
<!-- Include head links -->
<%@ include file="/WEB-INF/jspf/head.content.jspf"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Declare session userBean -->
<jsp:useBean id="userBean" class="model.UserBean" scope="session" />
<link type="text/css" rel="stylesheet"
	href="/HellowWorldFromEclipse/assets/vendor/Bootstrap/css/bootstrap.min.css" />
</head>
<body>
	<!-- Include header -->
	<%@ include file="/WEB-INF/jspf/header.content.jspf"%>
	<div class="container m-5">
		<div class="row">
			<div class="col-md-8">
				<!-- forEach loop to display all posts -->
				<c:forEach var="post" items="${listPost}">
					<div class="card mb-4">
						<div class="card-body">
							<h2 class="card-title">
								<c:out value="${post.title}" />
							</h2>
							<p class="card-text">
								<c:out value="${post.content}" />
							</p>
						</div>
						<div class="card-footer text-muted">
							Door: <c:out value="${post.user}" /><br>
							Datum: <c:out value="${post.date}" />
						</div>
					</div>
				</c:forEach>
			</div>
		    <div class="col-md-4">
				<div class="card" style="width: 18rem;">
				  <img class="card-img-top" src="./assets/img/logo.png" alt="SE">
				  <div class="card-body">
				    <p class="card-text">WebBlog made by Software-Essentials. For educational purposes only.</p>
				    <a href="./create" class="btn btn-primary">Create your own post</a>
				  </div>
				</div>
		    </div>
		</div>
	</div>
	

	<!-- Footer -->
	<%@ include file="/WEB-INF/jspf/footer.content.jspf"%>
</body>
</html>