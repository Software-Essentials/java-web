package util;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.LoginBean;
import model.UserBean;
//Login
public class LoginDao {
	private static Logger LOGGER = Logger.getLogger(LoginDao.class.getName());

	public static UserBean authenticateUser(LoginBean loginBean) throws Exception {
		try {
			//Make database connection
			Connection connection = DBConnection.getConnection();
			//Database query
			String query = "SELECT * FROM users WHERE username = ?";
			//Make statement
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			//set statement variables
			preparedStatement.setString(1, loginBean.getName());
			ResultSet resultSet = preparedStatement.executeQuery();
			//loop threw response to check
			while (resultSet.next()) {
				System.out.println("In while statement");
				if (loginBean.getPassword().equals(resultSet.getString("password"))) {
					//add data from database to userBean in session
					UserBean userBean = new UserBean();

					userBean.setUserName(loginBean.getName());
					userBean.setFirstName(resultSet.getString("firstname"));
					userBean.setLastName(resultSet.getString("lastname"));

					userBean.setRole(resultSet.getInt("role"));
					userBean.setID(resultSet.getInt("id"));
					//Create lastlogin time to save to database
					LocalDateTime currentDateTime = LocalDateTime.now();
					DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
					String formattedDateTime = currentDateTime.format(formatter);

					userBean.setLastLogin(formattedDateTime);
					//query to add last login time to database
					String query1 = "UPDATE users SET lastlogin = ? WHERE id = ?";
					// set all the preparedstatement parameters
					PreparedStatement st = connection.prepareStatement(query1);
					st.setString(1, userBean.getLastLogin());

					st.setInt(2, userBean.getID());

					// execute the preparedstatement insert
					st.executeUpdate();
					st.close();

					System.out.println(resultSet.getInt("id"));
					return userBean;
				}
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "Error while logging in" + e.getMessage());
		}
		throw new Exception("Error while logging in.");
	}
}