package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.UserBean;

public class RegisterDao {
	private static Logger LOGGER = Logger.getLogger(RegisterDao.class.getName());

	public static void registerUser(UserBean userBean) throws Exception {
		try {
			System.out.println("in try");
			Connection connection = DBConnection.getConnection();
			System.out.println("database connection");
			String query = "INSERT INTO users(firstname, lastname, username, password)" + " values (?, ?, ?, ?)";
			System.out.println("query made");
			// set all the preparedstatement parameters
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, userBean.getFirstName());
			st.setString(2, userBean.getLastName());
			st.setString(3, userBean.getUserName());
			st.setString(4, userBean.getPassword());
			System.out.println("added data to query");

			// execute the preparedstatement insert
			st.executeUpdate();
			st.close();
			System.out.println("Query excecuted");
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error while registering user" + e.getMessage());
		}
	}
}
