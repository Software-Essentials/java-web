package model;
package model;
//Userbean with getters and setters to acces user account
public class UserBean {

	private int id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private int role;
	private String lastlogin;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String newFirstName) {
		firstName = newFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String newLastName) {
		lastName = newLastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String newUsername) {
		username = newUsername;
	}

	public boolean isLoggedIn() {
		return (getFirstName().isEmpty()) ? false : true;
	}

	public int getRole() {
		return role;
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getLastLogin() {
		return lastlogin;
	}

	public void setLastLogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
}

public class UserBean {

	private int id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private int role;
	private String lastlogin;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String newFirstName) {
		firstName = newFirstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String newLastName) {
		lastName = newLastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		password = newPassword;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String newUsername) {
		username = newUsername;
	}

	public boolean isLoggedIn() {
		return (getFirstName().isEmpty()) ? false : true;
	}

	public int getRole() {
		return role;
	}

	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getLastLogin() {
		return lastlogin;
	}

	public void setLastLogin(String lastlogin) {
		this.lastlogin = lastlogin;
	}
}
